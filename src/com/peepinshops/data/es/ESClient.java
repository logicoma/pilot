package com.peepinshops.data.es;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.inject.Singleton;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.node.Node;

import static org.elasticsearch.node.NodeBuilder.*;

@Singleton
public class ESClient {

	static Node node;
	static Client client;
	
	static ESClient instance = null;
	
	private ESClient() {		
		System.out.println("init");
		node = nodeBuilder().node();
		client = node.client();
	}

	public static ESClient getInstance(){
		if (instance == null )
			instance = new ESClient();
		
		return instance;
		
	}
	public IndexResponse addDoc(String index, String type, String doc){
		return client.prepareIndex(index, type)
		        .setSource(doc)
		        .execute()
		        .actionGet();
	}
	
	public IndexResponse addDoc(String index, String type, Map<String, Object> doc){
		return client.prepareIndex(index, type)
		        .setSource(doc)
		        .execute()
		        .actionGet();
	}
	
	public void updateDoc(String index, String type, String id, Map<String, Object> upd) throws IOException, InterruptedException, ExecutionException{
		UpdateRequest updateRequest = new UpdateRequest().index(index).type(type).id(id);
		XContentBuilder updDoc = XContentFactory.jsonBuilder().startObject();
		for (Map.Entry<String, Object> entry : upd.entrySet()){
			updDoc.field(entry.getKey(), entry.getValue());
		}
		updateRequest.doc(updDoc.endObject()).refresh();
		client.update(updateRequest).get();
	}
	
	public SearchResponse searchDoc(String index, String type, 
			QueryBuilder query, FilterBuilder filter, int from, int size){
		SearchResponse response = client.prepareSearch(index)
		        .setTypes(type)
		        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
		        .setQuery(query)
		        .setPostFilter(filter)
		        .setFrom(from).setSize(size)
		        .execute()
		        .actionGet();
		return response;
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("finalize");
		super.finalize();
		node.close();
	}

	
}
