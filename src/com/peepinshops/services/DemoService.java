package com.peepinshops.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import com.peepinshops.data.es.ESClient;

@Path("/demo")
public class DemoService {

	ESClient esClient = ESClient.getInstance();

	static Logger logger = Logger.getLogger("MyLog");

	@POST
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response storeUser(Map<String, Object> params) {
		Map<String, Object> res = new HashMap<>();

		logger.info("user " + params);
		String name = "" + params.get("name");

		String token = Base64.getEncoder().encodeToString(
				(name + System.currentTimeMillis()).getBytes());
		params.put("token", token);

		ObjectMapper objectMapper = new ObjectMapper();

		String json;
		try {
			json = objectMapper.writeValueAsString(params);
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(400).build();
		}

		esClient.addDoc("entity", "demoProfile", json);
		res.put("token", token);

		return Response.ok(res).build();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("/recommendations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecommendations(@QueryParam("token") String userToken,
			@QueryParam("storeId") String storeId,
			@QueryParam("from") int from, @QueryParam("count") int size) {

		logger.info("recs " + userToken + " " + storeId);

		Map<String, Object> user = resolveUser(userToken);
		if (user == null) 
			return Response.status(Response.Status.FORBIDDEN).build();
		
		List<String> userTags = mapAttributesToTags((List<String>)user.get("tags"));
		
		logger.info(userTags.toString());
		
		QueryBuilder tagQuery = QueryBuilders.termsQuery("tags", userTags);
		
		FilterBuilder dislikeFilter = null;
		if (user.get("dislike") != null){
			dislikeFilter = FilterBuilders.notFilter(
				FilterBuilders.termsFilter("tags", mapDislikeToTags((List<String>)user.get("dislike"))));
		}
		
		SearchResponse taggedRecs = esClient.searchDoc("catalog", "tagged", tagQuery,
				dislikeFilter, from, size <= 0 ? 10 : size);
		
		Map<String, String> colorMap = new LinkedHashMap<String, String>();
		
		if (taggedRecs.getHits().getTotalHits() > 0){
			for (SearchHit hit : taggedRecs.getHits()){
				logger.info("tagged " + hit.getSourceAsString());
				colorMap.put("" + hit.getSource().get("externalId"), "" + hit.getSource().get("externalColor"));
			}
		}
		
		QueryBuilder query = QueryBuilders.boolQuery().must(
				QueryBuilders.termQuery("retailer.id", storeId));
		
		FilterBuilder filter = null;
		if (!colorMap.isEmpty()){
			filter = FilterBuilders.idsFilter("dress").addIds( (String[]) colorMap.keySet().toArray(new String[]{}));
		}

		SearchResponse result = esClient.searchDoc("catalog", "dress", query,
				filter, from, size <= 0 ? 10 : size);
		List<Map<String, Object>> res = new ArrayList<>();
		if (result.getHits().getHits().length > 0) {
			Iterator<SearchHit> items = result.getHits().iterator();
			while (items.hasNext()){
				Map<String, Object> item = items.next().getSource();
				item.put("preferedColor", colorMap.get("" + item.get("id")));
				res.add(item);
			}
		}
		return Response.ok(res).build();
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> resolveUser(String token) {
		SearchResponse resp = esClient.searchDoc("entity", "demoProfile",
				QueryBuilders.matchQuery("token", token), null, 0, 1);
		if (resp.getHits().totalHits() == 0)
			return null;

	//	return mapAttributesToTags((List<String>) .get("tags")
				return resp.getHits().iterator().next().getSource()/*)*/;
	}

private List<String> mapDislikeToTags(List<String> dislikes) {
		
		Set<String> accml = new HashSet<>();

		for (String dislike : dislikes) {
			switch (dislike) {
				case "mini":
					accml.add("mini_dress");
					break;
				case "midi":
					accml.add("midi_dress");
					break;
				case "maxi":
					accml.add("maxi_dress");
					break;
				case "shift":
					accml.add("shift_dress");
					break;
				case "fitflare":
					accml.add("fit_flare_skater");
					break;
				case "bodycon":
					accml.add("bodycon_dress");
					break;
				case "sheath":
					accml.add("sheath_dresses");
					break;
				default:
					accml.add(dislike);
			}
		}

		return new ArrayList<String>(accml);
	}
				
	
	private List<String> mapAttributesToTags(List<String> attributes) {
		
		Set<String> accml = new HashSet<>();

		for (String attr : attributes) {
			switch (attr) {
			case "tall":
				accml.add("shift_dress");
				accml.add("maxi_dress");
				accml.add("skater_dress");
				break;

			case "not_tall":
				accml.add("maxi_dress");
				accml.add("bodycon_dress");
				accml.add("baby_doll_dress");
				accml.add("skirt_dress");
				break;

			case "very_light":
				accml.add("not_very_bright_colors");
				break;

			case "light":
				accml.add("neutral_colors");
				accml.add("soft_colors");
				break;

			case "medium":
				accml.add("no_neutral_colors");
				break;

			case "dark":
				accml.add("not_very_bright_colors");
				break;

			case "very_dark":
				accml.add("very_bright_colors");
				break;

			case "small_boobs":
				accml.add("high_neckline");
				accml.add("turtle_neck");
				accml.add("off_shoulders");
				accml.add("halter_necks");
				accml.add("backless_dresses");
				accml.add("ruffle_necklines");
				accml.add("pleated_neckline");
				accml.add("strappy_neck");
				accml.add("chunky_knits");
				break;
				
			case "big_boobs":
				accml.add("v_neck");
				accml.add("scoop_neck");
				accml.add("one_shoulder_dresses");
				accml.add("sheath_dresses");
				break;
				
			case "round_tummy":
				accml.add("skater_dress");
				accml.add("skirt_dress");
				accml.add("shift_dress");
				accml.add("maxi_dress");
				accml.add("empire lose dresses");
				accml.add("a_line_dresses");
				accml.add("raised_waist_dresses");
				break;
				
			case "flat_tummy":
				accml.add("shift_dress");
				accml.add("bodycon_dress");
				accml.add("strapless");
				accml.add("skater_dress");
				accml.add("scalap_sleeve");
				accml.add("maxi_dress");
				accml.add("baby_doll_dress");
				accml.add("empire tight dress");
				break;
				
			case "straight_hips":
				accml.add("shift_dress");
				accml.add("maxi_dress");
				accml.add("skater_dress");
				accml.add("bodycon_dress");
				accml.add("tunics");
				break;
				
			case "curvey_hips":
				accml.add("bodycon_dress");
				accml.add("skirt_dress");
				accml.add("baby_doll_dress");
				accml.add("maxi_dress");
				accml.add("wrap_dresses");
				break;
				
			case "hide_arms":
				accml.add("full_sleeve");
				accml.add("3_4_sleeve");
				accml.add("half_sleeve");
				accml.add("loose_sleeve");
				accml.add("cap_sleeve");
				accml.add("mix");
				accml.add("tight_sheer_sleeve");
				break;
				
			case "showoff_arms":
				accml.add("without_sleeve");
				accml.add("tank_top");
				accml.add("mix");
				accml.add("cap_sleeve");
				accml.add("tight_sheer_sleeve");
				break;
				
			case "hide_legs":
				accml.add("above_the_knee");
				accml.add("knee_length");
				accml.add("long");
				accml.add("mix");
				accml.add("a_line_dresses");
				break;
				
			case "showoff_legs":
				accml.add("above_the_knee");
				accml.add("knee_length");
				accml.add("long");
				accml.add("mix");
				break;
				
			case "hide_neckline":
				accml.add("scoop_neck");
				accml.add("backless_dresses");
				accml.add("ruffle_necklines");
				accml.add("high_neckline");
				accml.add("pleated_neckline");
				accml.add("halter_necks");
				accml.add("turtle_neck");
				break;
				
			case "showoff_neckline":
				accml.add("v_neck");
				accml.add("strappy_neck");
				break;
				
			case "tight":
				accml.add("bodycon_dress");
				accml.add("maxi_dress");
				accml.add("mini_dress");
				accml.add("midi_dress");
				break;
				
			case "lose":
				accml.add("shift_dress");
				accml.add("fit_flare_skater");
				accml.add("skater_dress");
				accml.add("maxi_dress");
				accml.add("mini_dress");
				accml.add("midi_dress");
				accml.add("skirt_dress");
				accml.add("baby_doll_dress");
				break;
				
			case "neutrals":
				accml.add("browns");
				accml.add("beige");
				accml.add("black");
				accml.add("white");
				accml.add("gray");
				accml.add("sometimes_greens");
				break;
				
			case "bright_colors":
				accml.add("everything_non_earthy_colors");
				break;
				
			case "bothcolors":
				accml.add("all_colors");
				break;
				
			case "silver_tones":
				accml.add("silver_tones");
				break;
				
			case "gold_tones":
				accml.add("gold_tones");
				break;
				
			case "mixed_tones":
				accml.add("gold_tones");
				break;
				
			default:
				// "dress_20-50 etc
				accml.add(attr);
			}
		}

		return new ArrayList<String>(accml);
	}

	@GET
	@Path("/item/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItem(@PathParam("id") String itemId) {

		logger.info("item " + itemId);

		FilterBuilder filter = FilterBuilders.idsFilter("dress").ids(itemId);
		SearchResponse result = esClient.searchDoc("catalog", "dress", null,
				filter, 0, 1);
		Map<String, Object> product = new HashMap<>();
		if (result.getHits().getHits().length > 0)
			product = result.getHits().getAt(0).getSource();

		return Response.ok(product).build();
	}
}
