package com.peepinshops.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.peepinshops.data.es.ESClient;

@Path("catalog")
public class CatalogTagger {

	//@Inject
	ESClient esClient = ESClient.getInstance();
	
	static Logger logger = Logger.getLogger("MyLog");
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
	public static int counter = 0;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Map<String, Object> next(@QueryParam("retailerId") String retailerId) {
    	
    	QueryBuilder query = QueryBuilders.boolQuery()
    			.must(QueryBuilders.termQuery("retailer.id", retailerId));
    	FilterBuilder filter = FilterBuilders.andFilter(
    			FilterBuilders.missingFilter("tags"),
    			FilterBuilders.missingFilter("color_tags"));
    	
    	SearchResponse result = esClient.searchDoc("catalog", "dress", query, filter, 0, 1);
    	
    	List<String> products = new ArrayList<String>();
    	products.add(result.getHits().getAt(0).getSourceAsString());
    	
    	Map<String, Object> resMap = new HashMap<>();
    	resMap.put("products", products);
    	resMap.put("left", result.getHits().getTotalHits());
    	
        return resMap;
    }
    
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public String postIt(InputStream incomingData) {
    	StringBuilder incomingString = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null) {
            	incomingString.append(line);
            }
        } catch (Exception e) {
            logger.info("Error Parsing: - ");
        }
        logger.info("Data Received: " + incomingString.toString()); //if (true) return "";
        
        try {
			@SuppressWarnings("unchecked")
			Map<String,Object> result =
			        new ObjectMapper().readValue(incomingString.toString(), HashMap.class);
			String id = String.valueOf(result.get("id"));
			Map<String, Object> doc = esClient.searchDoc("catalog", "dress", null, 
					FilterBuilders.idsFilter("dress").addIds(id), 0, 1).getHits().hits()[0].getSource();
			
			Map<String, List<String>> colorTags = (Map<String, List<String>>)result.get("color_tags");
			List<String> tags = (List<String>)result.get("tags");
			float price = doc.get("salePrice") != null && StringUtils.isNotEmpty("" + doc.get("salePrice")) ? 
					Float.valueOf("" + doc.get("salePrice")) : Float.valueOf("" + doc.get("price"));
			
			if (price <= 50) tags.add("dress_20_50");
			if (price >= 50 && price <= 100) tags.add("dress_50_100");
			if (price >= 100 && price <= 150) tags.add("dress_100_150");
			if (price >= 150 && price <= 200) tags.add("dress_150_200");
			if (price >= 200) tags.add("dress_200_above");
			
			if (colorTags.isEmpty())
			{
				Map<String, Object> newDoc = new HashMap<>();
				newDoc.put("externalId", id);
				newDoc.put("tags", tags);
				esClient.addDoc("catalog", "tagged", newDoc);
			}
			else {
				for (Map.Entry<String, List<String>> colorTagsEntry : colorTags.entrySet()){
					Map<String, Object> newDoc = new HashMap<>();
					newDoc.put("externalId", id);
					List<String> resTags = new ArrayList<>();
					resTags.addAll(tags);
					resTags.addAll(colorTagsEntry.getValue());
					newDoc.put("tags", resTags);
					newDoc.put("externalColor", colorTagsEntry.getKey());
					esClient.addDoc("catalog", "tagged", newDoc);
				}
			}
			
			System.out.println(result);
		//	if (false)
				esClient.updateDoc("catalog", "dress", String.valueOf(result.get("id")), result);
		} catch (IOException | InterruptedException | ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        // the following statement is used to log any messages  
        logger.info(incomingString.toString());
 
        return "";
    }
}
