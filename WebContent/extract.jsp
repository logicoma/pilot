<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
<script>
	var retailer = 'r36'; //r36 - forever21, r2 - macy's
	var offsetM = 0;
	var offsetF = 0;

	function catItem($scope, $http) {

		var total = 1329; //check total at shopsense

		for (var offset = 0; offset < total; offset++) {
			var url = 'http://api.shopstyle.com/api/v2/products?pid=uid7344-26492812-85&cat=dresses&limit=1&sort=Popular&fl=r36';
			url += '&offset=' + offset;

			var config = {
				headers : {
					'Access-Control-Allow-Origin' : '*'
				}
			};

			$http.get(url).success(
					function(data) {
						total = data.metadata.total;
						$http.post('http://127.0.0.1:9200/catalog/dress/'
								+ data.products[0].id, data.products[0])
					});
		}
	}
</script>
</head>

<body>
	<div ng-controller="catItem"></div>
	<p />

</body>
</html>