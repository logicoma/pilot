<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<title>Internal, Catalog metadata</title>
<style>
table {
	width: 100%;
	border-width: thin;
	border-style: solid;
	border-color: grey;
	border-collapse: collapse;
}

td, th {
	border: 1px solid grey
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>
<script>
	var retailer = '2';
	var offsetM = 0;
	var offsetF = 0;

	var itemTags = {
		"shift_dress" : "Shift dress",
		"maxi_dress" : "Maxi dress",
		"skater_dress" : "Skater dress",
		"bodycon_dress" : "bodycon dress",
		"baby_doll_dress" : "baby doll dress",
		"skirt_dress" : "Skirt dress",
		"one_shoulder_dresses" : "one shoulder dresses",
		"sheath_dresses" : "sheath dresses",
		"a_line_dresses" : "A-line dresses",
		"raised_waist_dresses" : "Raised-waist dresses",
		"tunics" : "tunics",
		"wrap_dresses" : "wrap dresses",
		"backless_dresses" : "backless dresses",
		"empire_lose_dresses" : "empire lose dresses",
		"empire_tight_dresses" : "empire tight dresses",
		"high_neckline" : "high neckline",
		"turtle_neck" : "turtle neck",
		"off_shoulders" : "off shoulders",
		"halter_necks" : "halter necks",
		"ruffle_necklines" : "Ruffle necklines",
		"pleated_neckline" : "pleated neckline",
		"strappy_neck" : "Strappy neck",
		"chunky_knits" : "chunky knits",
		"v_neck" : "V neck",
		"scoop_neck" : "Scoop neck",
		"strapless" : "strapless",
		"scalap_sleeve" : "scalap sleeve",
		"full_sleeve" : "full sleeve",
		"3_4_sleeve" : "3/4 sleeve",
		"half_sleeve" : "half sleeve",
		"loose_sleeve" : "loose sleeve",
		"cap_sleeve" : "cap sleeve",
		"mix" : "mix",
		"tight_sheer_sleeve" : "tight/sheer sleeve",
		"without_sleeve" : "without sleeve",
		"tank_top" : "tank top",
		"above_the_knee" : "Above the knee",
		"knee_length" : "knee length",
		"long" : "long",
		"mini_dress" : "Mini dress",
		"midi_dress" : "Midi dress",
		"fit_flare_skater" : "fit & Flare skater",
	};

	var colorTags = {
		'browns' : 'Browns',
		'beige' : 'Beige',
		'black' : 'Black',
		'white' : 'White',
		'gray' : 'Gray',
		'sometimes_greens' : 'Sometimes Greens',
		'everything_non_earthy_colors' : 'Everything Non Earthy Colors',
		'not_very_bright_colors' : 'Not Very Bright Colors',
		'neutral_colors' : 'Neutral Colors',
		'soft_colors' : 'Soft Colors',
		'no_neutral_colors' : 'No Neutral Colors',
		'very_bright_colors' : 'Very Bright Colors',
		'all_colors' : 'All Colors',
		'silver_tones' : 'Silver Tones',
		'gold_tones' : 'Gold Tones',
		'mix_tones' : 'Mix Tones',
		'print_colors' : 'Print Colors'
	};

	var result = {};
	result.tags = [];
	result.color_tags = {};
	function catItem($scope, $http) {

		$scope.retailer = retailer;
		$scope.offsetM = offsetM;
		$scope.offsetF = offsetF;

		$scope.itemTags = itemTags;
		$scope.colorTags = colorTags;

		//$scope.result = result;

		$scope.toggleSelection = function toggleSelection(key, tag, id) {
			if (id !== undefined) {
				result.id = id;
				toggleTags(result.tags, tag);
			} else {
				if (result.color_tags[key] === undefined) {
					result.color_tags[key] = [];
				}
				toggleTags(result.color_tags[key], tag)
			}

			$scope.result = result;
		};

		function toggleTags(arr, tag) {
			{
				var index = arr.indexOf(tag);
				if (index === -1) {
					arr.push(tag);
				} else {
					arr.splice(index, 1);
				}
			}
			return arr;
		}
		$scope.proceed = function(itemId) {
			//if ($scope.result.length > 0)

		 	if ($scope.result == undefined)
		 		fetchNext();
			else {
				$scope.result.id = itemId;
				var resJson = $scope.result;

				$http.post('api/catalog', resJson, {
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(res) {
					$scope.test = res.config.data;
					fetchNext();
				});
			}
		}
		
		function fetchNext(){
			result = {};
			result.tags = [];
			result.color_tags = {};
			//wtf?
			$scope.result = undefined;
			
			var url = 'api/catalog?retailerId=' + $scope.retailer;

			$http.get(url).success(function(data) {
				$scope.retail_data = data;
				$scope.retail_data.products[0] = JSON.parse(data.products[0]);
				
				$scope.itemTags = itemTags;
				
				$scope.$apply();
			});
		}

		$scope.proceed();

	}
</script>
</head>

<body>
	<div ng-controller="catItem">
		<p>
			<b>{{retail_data.products[0].retailer.name}}: </b>
			{{retail_data.products[0].brand.name}}
		</p>
		<hr />
		<p>{{retail_data.products[0].name}}</p>
		<hr />


		<table>
			<tr>
				<td><span
					ng-bind-html-unsafe="retail_data.products[0].description"></span>
					<p>Price: {{retail_data.products[0].priceLabel}}</p>
					<p>On Sale Price:{{retail_data.products[0].salePriceLabel}}</p> <br />
					<img ng-src="{{retail_data.products[0].image.sizes.XLarge.url}}" />
					<img ng-repeat="aimg in retail_data.products[0].alternateImages"
					ng-src="{{aimg.sizes.XLarge.url}}" /> <br /></td>
					
				<td><span ng-if="retail_data.products[0].description"><label ng-repeat="(key,label) in itemTags"> <input
						type="checkbox" name="{{retail_data.products[0].id}}[]" id="{{retail_data.products[0].id}}"
						value="{{key}}"
						ng-click="toggleSelection('tags', key, retail_data.products[0].id)">{{label}}<br />
				</label></span></td>
			</tr>
			<tr>
				<td colspan=2><b>Colors:</b></td>
			</tr>

			<tr ng-repeat="colorOpt in retail_data.products[0].colors"
				style="border-width: 1px;">
				<td>{{colorOpt.name}}: <img ng-src="{{colorOpt.swatchUrl}}" /><br />
					<img ng-src="{{colorOpt.image.sizes.XLarge.url}}" />
				</td>
				<td><label ng-repeat="(key,label) in colorTags"> <input
						type="checkbox" name="{{colorOpt.name}}[]" value="{{key}}"
						ng-click="toggleSelection(colorOpt.name, key)">{{label}}<br />
				</label></td>
			</tr>

		</table>
		<p style="border-width: thin;">
		<div>
			<div ng-show="retailer == '2' ">Macy's current item: {{1295 -
				retail_data.left}}</div>
			<div ng-show="retailer == '36'">Forever21 current item: {{1329
				- retail_data.left}}</div>

		</div>
		<br /> Fetch next item from: <span> Macy's<input type="radio"
			ng-model="retailer" value="2"> Forever21<input type="radio"
			ng-model="retailer" value="36">
		</span>
		<button ng-click="proceed(retail_data.products[0].id)">Save
			tags and Proceed to next item</button>
		<br />
	</div>
	<p />

</body>
</html>